﻿using DemoWebShop.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace DemoWebShop.Hook
{
    [Binding]
    public class Hook:WebDriver
    {
        [AfterScenario]
        public void AfterScenario() => Quit();
       
    }
}
