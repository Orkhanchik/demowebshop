﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWebShop.Interface
{
    public interface IRegistration
    {
        /// <summary>
        /// Opens registration page
        /// </summary>
        /// <param name="page_name"></param>
        void OpenPage();

        /// <summary>
        /// Fills registrtation page with data
        /// </summary>
        /// <param name="first_name"></param>
        /// <param name="last_name"></param>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <param name="confirm_password"></param>
        void RegisterUserWithData(string first_name, string last_name, string email, string password, string confirm_password, string gender);

        /// <summary>
        /// Chooses gender
        /// </summary>
        /// <param name="gender_male"></param>
        void SelectGender(string gender_male);

        /// <summary>
        /// Displays error message if registration failed
        /// </summary>
        string DisplayErrorMessage(string error_msg);

        string DisplayError(string error_msg);

        string DisplayRegisterMessage();

    }
}