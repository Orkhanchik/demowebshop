﻿using DemoWebShop.Helper;
using OpenQA.Selenium;

namespace DemoWebShop.POM
{
    public class NotebooksPage : WebDriver
    {

        private By _AddToCartBtn = By.XPath("//input[@value='Add to cart']");
        private string _notebooksPageUrl = "http://demowebshop.tricentis.com/notebooks";
        public void OpenNotebooksPage()
        {
            Navigate().GoToUrl(_notebooksPageUrl);
        }
        public void AddToCartClick()
        {
            FindElement(_AddToCartBtn).Click();
        }
    }
}
