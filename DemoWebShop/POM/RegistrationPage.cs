﻿using DemoWebShop.Helper;
using OpenQA.Selenium;

namespace DemoWebShop.POM
{
    public class RegistrationPage : WebDriver
    {
        

        private string _registrationPage = "http://demowebshop.tricentis.com/register";
        private By _genderMaleRadioButton = By.Id("gender-male");
        private By _genderFemaleRadioButton = By.Id("gender-female");
        private By _firstNameField = By.Id("FirstName");
        private By _lastNameField = By.Id("LastName");
        private By _emailField = By.Id("Email");
        private By _passwordField = By.Id("Password");
        private By _confirmPasswordField = By.Id("ConfirmPassword");
        private By _registerBtn = By.Id("register-button");
        private By _registerMessage = By.XPath("//div[@class='result']");

        private By _firstNameErrorText = By.XPath("//span[@for='FirstName']");
        private By _lastNameErrorText = By.XPath("//span[@for='LastName']");
        private By _emailErrorText = By.XPath("//span[@for='Email']");
        private By _emailWrongErrorText = By.XPath("//span[@class='field-validation-error']");
        private By _emailExistErrorText = By.XPath("//div[@class = 'validation-summary-errors']//ul/li");
        private By _passwordErrorText = By.XPath("//span[@for='Password']");
        private By _ConfirmPasswordErrorText = By.XPath("//span[@for='ConfirmPassword']");

        public void OpenPage()
        {
            Navigate().GoToUrl("http://demowebshop.tricentis.com/register");
        }

        public void SendFirstName(string first_name)
        {
            FindElement(_firstNameField).SendKeys(first_name);
        }

        public void SendLastName(string last_name)
        {
            FindElement(_lastNameField).SendKeys(last_name);
        }

        public void SendEmail(string email)
        {
            FindElement(_emailField).SendKeys(email);
        }

        public void SendPassword(string password)
        {
            FindElement(_passwordField).SendKeys(password);
        }

        public void SendConfirmPassword(string confirm_password)
        {
            FindElement(_confirmPasswordField).SendKeys(confirm_password);
        }

        public void ClickOnRegisterBtn()
        {
            FindElement(_registerBtn).Click();
        }

        public string DisplayRegisterMessage()
        {
            return FindElement(_registerMessage).Text;
        }

        public void SelectGender(string gender)
        {
            if (gender == "male")
            {
                FindElement(_genderMaleRadioButton).Click();
            }
            else
            {
                FindElement(_genderFemaleRadioButton).Click();
            }
        }

        public string GetErrorMessage(string error_msg)
        {

            switch (error_msg)
            {
                case "First name is required":
                    return FindElement(_firstNameErrorText).Text;

                case "Last name is required":
                    return FindElement(_lastNameErrorText).Text;

                case "Email is required":
                    return FindElement(_emailErrorText).Text;

                case "Wrong email":
                    return FindElement(_emailWrongErrorText).Text;

                case "Password is required":
                    return FindElement(_passwordErrorText).Text;

                case "The password and confirmation password do not match.":
                    return FindElement(_ConfirmPasswordErrorText).Text;

                case "The specified email already exists.":
                    return FindElement(_emailExistErrorText).Text;

                default:
                    break;
            }

            return null;
        }


    }
}
