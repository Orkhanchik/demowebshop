﻿using DemoWebShop.Helper;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWebShop.POM
{
    class ApparelShoesPage:WebDriver
    {
        private string _apparelShoesPageUrl = "";
        private By _addToCardbtn = By.XPath("//div[@class='item-box'][6]/div/div[2]/div[3]/div[2]/input");

        public void OpenApparelShoesPage()
        {
            Navigate().GoToUrl(_apparelShoesPageUrl);
        }

        public void ClickOnAddtoCard()
        {
            FindElement(_addToCardbtn).Click();
        }
    }
}