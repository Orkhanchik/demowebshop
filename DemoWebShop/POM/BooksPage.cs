﻿using DemoWebShop.Helper;
using OpenQA.Selenium;

namespace DemoWebShop.POM
{
   public class BooksPage : WebDriver
    {
        private By _addToCartComputingandInternet = By.XPath("//div[@class='tem-box'][1]");
        private string _booksPage = "http://demowebshop.tricentis.com/books";


        private void OpenBookPage()
        {
            Navigate().GoToUrl(_booksPage);
        }

        public void AddComputingandInternetToCart()
        {
            FindElement(_addToCartComputingandInternet).Click();
        }
    }
}