﻿using DemoWebShop.Helper;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWebShop.POM
{
    public class GiftCardsPage : WebDriver
    {
        private string _giftCardsPageUrl = "http://demowebshop.tricentis.com/gift-cards";
        private By _physicalGiftCard100 = By.XPath("//img[@alt='Picture of $100 Physical Gift Card']");
        private By _getGiftCardsName = By.XPath("//h2[@class='product-title']/a");

        public void ClickOnCard100()
        {
            FindElement(_physicalGiftCard100).Click();

        }
        public void OpenCardsPage()
        {
            Navigate().GoToUrl(_giftCardsPageUrl);

        }

        //public List<string> GetNameGiftCards()
        //{
        //    List<string> names = new List<string>();

        //    foreach (var item in FindElements(_getGiftCardsName))
        //    {
        //        names.Add(item.Text);
        //    }
        //    return names;
        //}

    }
}
