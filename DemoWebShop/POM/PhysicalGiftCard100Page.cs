﻿using DemoWebShop.Helper;
using OpenQA.Selenium;

namespace DemoWebShop.POM
{
    public class PhysicalGiftCard100Page : WebDriver
    {
        HelperMethods HelperMethods = new HelperMethods();
        private By _recipientsNameField = By.XPath("//input[@class='recipient-name']");
        private By _yourNameField = By.XPath("//input[@class='sender-name valid']");
        private By _addToCartBtn = By.XPath("//input[@id='add-to-cart-button-4']");

        public void SendDataToYourNameField()
        {
            FindElement(_yourNameField).SendKeys(HelperMethods.GetRandomValidMail());
        }
        public void SendDataToRecipientsNameField()
        {
            FindElement(_recipientsNameField).SendKeys(HelperMethods.GetRandomValidMail());
        }

        public void ClickOnAddToCartBtn()
        {
            FindElement(_addToCartBtn).Click();
        }
    }
}
