﻿using DemoWebShop.Helper;
using OpenQA.Selenium;
namespace DemoWebShop.POM
{
    public class LoginPage : WebDriver
    {
        private string _loginPageUrl = "http://demowebshop.tricentis.com/login";
        private By _emailField = By.XPath("//input[@autofocus='autofocus']");
        private By _passwordField = By.XPath("//input[@class='password']");
        private By _loginBtn = By.XPath("//input[@class='button-1 login-button']");
        private By _LoginText = By.XPath("//div[@class='header-links'][1]/ul/li/a");

        private By _errorMessageForEmail = By.XPath("//div[@class='validation-summary-errors']/span");
        private By _errorMessageForWrongEmail = By.XPath("//span[@for='Email']");


        public string GetTextFromLogin()
        {
            return FindElement(_LoginText).Text;
        }
        public string GetErrorMessage(string errorMessage)
        {
            switch (errorMessage)
            {
                case "No customer account found":
                    return FindElement(_errorMessageForEmail).Text;
                case "The credentials provided are incorrect":
                    return FindElement(_errorMessageForEmail).Text; ;
                case "Please enter a valid email address.":
                    return FindElement(_errorMessageForWrongEmail).Text;

                default:
                    return null;
            }
        }
        public void OpenLoginPage()
        {
            Navigate().GoToUrl(_loginPageUrl);

        }

        public void SendEmailToEmailField(string email)
        {
            FindElement(_emailField).SendKeys(email);
        }

        public void SendPasswordToPasswordField(string password)
        {
            FindElement(_passwordField).SendKeys(password);

        }

        public void ClickOnLoginBtn()
        {
            FindElement(_loginBtn).Click();
        }
    }
}
