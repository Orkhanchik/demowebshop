﻿using DemoWebShop.Helper;
using DemoWebShop.Implement.General;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace DemoWebShop.Steps
{
    [Binding]
    public class RegistrationSteps
    {
        [Given(@"Page '(.*)' is opened")]
        public void GivenPageIsOpened(string pageName)
        {
            PagesOpener pagesOpener = new PagesOpener();
            pagesOpener.OpenPage(pageName);
        }

        [When(@"Registrtation form is sent with data")]
        public void WhenRegistrtationFormIsSentWithData(Table table)
        {
            GeneralInterfaces.RegistrationGui.RegisterUserWithData(
                table.Rows[0]["firstName"],
                table.Rows[0]["lastName"],
                table.Rows[0]["email"],
                table.Rows[0]["password"],
                table.Rows[0]["confirmPassword"],
                table.Rows[0]["gender"]);
        }
        [Then(@"'(.*)' message is displayed")]
        public void ThenMessageIsDisplayed(string exp)
        {
            Assert.AreEqual(exp, GeneralInterfaces.RegistrationGui.DisplayRegisterMessage());
        }

        [Then(@"Error message with text '(.*)' is displayed")]
        public void ThenErrorMessageWithTextIsDisplayed(string error_message)
        {
            switch (error_message.ToLower())
            {
                case "first name is required.":
                    Assert.AreEqual(error_message, GeneralInterfaces.RegistrationGui.DisplayErrorMessage(error_message));
                    break;
                case "last name is required.":
                    Assert.AreEqual(error_message, GeneralInterfaces.RegistrationGui.DisplayErrorMessage(error_message));
                    break;
                case "email name is required.":
                    Assert.AreEqual(error_message, GeneralInterfaces.RegistrationGui.DisplayErrorMessage(error_message));
                    break;
                case "wrong Email.":
                    Assert.AreEqual(error_message, GeneralInterfaces.RegistrationGui.DisplayErrorMessage(error_message));
                    break;
                case "the password and confirmation password do not match.":
                    Assert.AreEqual(error_message, GeneralInterfaces.RegistrationGui.DisplayErrorMessage(error_message));
                    break;
                case "the specified email already exists.":
                    Assert.AreEqual(error_message, GeneralInterfaces.RegistrationGui.DisplayErrorMessage(error_message));
                    break;

            }
        }


    }
}
