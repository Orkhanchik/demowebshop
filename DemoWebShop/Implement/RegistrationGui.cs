﻿using DemoWebShop.Helper;
using DemoWebShop.Interface;
using DemoWebShop.POM;

namespace DemoWebShop.Implement
{
    public class RegistrationGui : IRegistration
    {
        RegistrationPage registrationPage;
        public RegistrationGui()
        {
            registrationPage = new RegistrationPage();
        }
        public string DisplayErrorMessage(string error_msg)
        {
            return registrationPage.GetErrorMessage(error_msg);
        }

        public string DisplayRegisterMessage()
        {
            return registrationPage.DisplayRegisterMessage();
        }

        public void OpenPage()
        {
            registrationPage.OpenPage();
        }

        public void RegisterUserWithData(string first_name, string last_name, string email, string password, string confirm_password, string gender)
        {
            HelperMethods helperMethods = new HelperMethods();
            registrationPage.SendFirstName(first_name);
            registrationPage.SendLastName(last_name);

            if (email== "RandomValidMail")
            {
                email = helperMethods.GetRandomValidMail();
            }
            registrationPage.SendEmail(email);
            registrationPage.SendPassword(password);
            registrationPage.SendConfirmPassword(confirm_password);
            registrationPage.ClickOnRegisterBtn();
        }

        public void SelectGender(string gender_male)
        {
            registrationPage.SelectGender(gender_male);
        }

        public string DisplayError(string error_msg)
        {
            return registrationPage.GetErrorMessage(error_msg);
        }

    }
}