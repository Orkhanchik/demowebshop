﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace DemoWebShop.Helper
{
   public class WebDriver
    {
        private static IWebDriver driver;
        private static WebDriverWait wait;
        public IWebDriver GetDriver()
        {
            if (!ScenarioContext.Current.ContainsKey("driver"))
            {
                driver = CreateWebDriver();
                ScenarioContext.Current["driver"] = driver;
            }
            return driver;
        }

        public WebDriverWait GetWaiter()
        {
            if (!ScenarioContext.Current.ContainsKey("wait"))
            {
                wait = new WebDriverWait(GetDriver(), TimeSpan.FromSeconds(60));
                ScenarioContext.Current["wait"] = wait;
            }
            return wait;
        }

        public string Url
        {
            get { return driver.Url; }
            set { driver.Url = value; }
        }

        public string Title => GetDriver().Title;

        public string PageSource => GetDriver().PageSource;

        public string CurrentWindowHandle => GetDriver().CurrentWindowHandle;

        public ReadOnlyCollection<string> WindowHandles => GetDriver().WindowHandles;

        public void Close() => GetDriver().Close();

        public void Dispose() => GetDriver().Dispose();

        /// <summary>
        /// This method finds element and waits while this element will be visible and (or) clickable 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns>
        /// OpenQA.Selenium.IWebElement
        /// </returns>
        public IWebElement FindElement(By locator)
        {
            try
            {
                return GetDriver().FindElement(WaitСlickability(locator));
            }
            catch
            {
                return GetDriver().FindElement(WaitVisibility(locator));
            }
        }

        /// <summary>
        /// This method finds all elements and waits while this element will be visible and (or) clickable 
        /// </summary>
        /// <param name="locator"></param>
        /// <returns>
        /// OpenQA.Selenium.ReadOnlyCollection<IWebElement>
        /// </returns>
        public ReadOnlyCollection<IWebElement> FindElements(By locator)
        {
            try
            {
                return GetDriver().FindElements(WaitСlickabilityAllElements(locator));
            }
            catch
            {
                return GetDriver().FindElements(WaitVisibilityOfAllElements(locator));
            }
        }

        public IOptions Manage() => GetDriver().Manage();


        public INavigation Navigate() => GetDriver().Navigate();

        public void Quit()
        {
            GetDriver().Quit();
        }

        public ITargetLocator SwitchTo() => GetDriver().SwitchTo();

        private IWebDriver CreateWebDriver()
        {
            IWebDriver driver = new FirefoxDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(60);
            driver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(60);
            ScenarioContext.Current["driver"] = driver;
            return driver;
        }

        public By WaitVisibility(By locator)
        {
            GetWaiter().Until(ExpectedConditions.ElementIsVisible(locator));
            return locator;
        }

        public By WaitVisibilityOfAllElements(By locator)
        {
            GetWaiter().Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(locator));
            return locator;
        }

        public By WaitСlickability(By locator)
        {
            GetWaiter().Until(ExpectedConditions.ElementToBeClickable(locator));
            return locator;
        }

        public By WaitСlickabilityAllElements(By locator)
        {
            foreach (var elem in GetDriver().FindElements(locator).ToList())
            {
                GetWaiter().Until(ExpectedConditions.ElementToBeClickable(elem));
            }
            return locator;


        }
    }
}
