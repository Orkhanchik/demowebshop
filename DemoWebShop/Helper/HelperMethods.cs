﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWebShop.Helper
{
   public class HelperMethods
    {
        
        public string GetRandomValidMail()
        {
            
            string email="";
            for (int i = 0; i < 10; i++)
            {
                email = email + GetValidWord();
            }
            
            return email+"@gmail.com"; 
        }
        public string GetValidWord()
        {
            Random random = new Random();
            int a = random.Next(1, 10);
            if (a==1)
            {
                return "a";
            }
            else if (a == 2)
            {
                return "d";
            }
            else if (a == 3)
            {
                return "o";
            }
            else if (a == 4)
            {
                return "s";
            }
            else if (a == 5)
            {
                return "f";
            }
            else if (a == 6)
            {
                return "g";
            }
            else if (a == 7)
            {
                return "h";
            }
            else if (a == 8)
            {
                return "j";
            }
            else if (a == 9)
            {
                return "k";
            }
            return "l";

        }

    }
}
