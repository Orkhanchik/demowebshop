﻿using DemoWebShop.POM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWebShop.Helper
{
   public class PagesOpener
    {
        LoginPage login = new LoginPage();
        RegistrationPage RegistrationPage = new RegistrationPage();
        public void OpenPage(string pageName)
        {
            switch (pageName)
            {
                case "Login Page":
                    login.OpenLoginPage();
                    break;
                case "Registration page":
                    RegistrationPage.OpenPage();
                    break;

                default:
                    break;
            }
        }
    }
}
