﻿Feature: CheckRegistration
	In order to have more functional opportunities 
	As a customer
	I want to have an opportunity to register my account

Scenario: It is possible to register an account
	Given Page 'Registration page' is opened
	When Registrtation form is sent with data
		| firstName | lastName  | email           | password   | confirmPassword | gender |
		| Timur     | Ismayilov | RandomValidMail | $diablo666 | $diablo666      | male   |
	Then 'Your registration completed' message is displayed

Scenario Outline: It is impossible to register an account
	Given Page 'Registration page' is opened
	When Registrtation form is sent with data
		| firstName    | lastName  | email   | password   | confirmPassword  | gender   |
		| <firstName> | <lastName> | <email> | <password> | <confirmPassword> | <gender> |
	Then Error message with text '<ErrorMessage>' is displayed

	Examples:
		| firstName | lastName  | email                 | password   | confirmPassword | errorMessage                                         | gender |
		|           | Ismayilov | RandomValidMail       | $diablo666 | $diablo666      | First name is required.                              | male   |
		| Timur     |           | RandomValidMail       | $diablo666 | $diablo666      | Last name is required.                               | male   |
		| Timur     | Ismayilov |                       | $diablo666 | $diablo666      | Email is required.                                   | Female |
		| Timur     | Ismayilov | aabababs              | $diablo666 | $diablo666      | Wrong email.                                         | Female |
		| Timur     | Ismayilov | RandomValidMail       |            | $diablo666      | Password is required                                 | Female |
		| Timur     | Ismayilov | RandomValidMail       | aabababs   | $diablo666      | The password and confirmation password do not match. | Female |
		| Timur     | Ismayilov | RandomValidMail       | $diablo666 |                 | Password is required.                                | Female |
		| Timur     | Ismayilov | RandomValidMail       | $diablo666 | aabababs        | The password and confirmation password do not match. | Female |
		| Timur     | Ismayilov | ismaylovt92@gmail.com | $diablo666 | aabababs        | The specified email already exists.                  | Female |