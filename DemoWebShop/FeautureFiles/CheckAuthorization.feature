﻿    Feature: CheckAuthorizations 
	In order to check changes and details in my account
	As a customer
	I want to have authorization option

@mytag
Scenario: It is possible to authorize with valid data
	Given Page 'Login Page' is opened
	When Authorization form is sent with data
		| Email                 | Password   |
		| ismaylovt92@gmail.com | $diablo666 |
	Then User is successfully authorized

Scenario Outline: It is impossible to authorize with invalid data
	Given Page 'Login Page' is opened
	When Authorization form is sent with data
		| Email   | Password   |
		| <Email> | <Password> |
	Then Error message with text '<ErrorMessage>' is displayed

	Examples:
		| Email                 | Password   | ErrorMessage                           |
		|                       | $diablo666 | No customer account found              |
		| ismaylovt92@gmail.com | asdasd     | The credentials provided are incorrect |
		| adasdasd              | $diablo666 | Please enter a valid email address.    |
		| ismaylovt92@gmail.com |            | The credentials provided are incorrect |
		| RandomValidMail       | dfsdbfsda  | No customer account found              |
